## OpenSim Libre Panel

This is a free open source panel for managing OpenSimulator Grids and Standalones
that is offered under the GPL v3 License and built off of the PHP framework
[CodeIgniter](https://codeigniter.com/).



