<?php
class Admin_model extends CI_Model {

    public function register_admin(){
        $this->load->helper('string'); // Tool for creating strings
        $this->load->library('uuidgen'); // Library for generating uuids
        $sitedb = $this->load->database('sitedb', TRUE); // Establish connection with database

        // Pull in the submitted data
        $name = $this->input->post('fullname');
        $email = $this->input->post('emailaddress');
        $type = $this->input->post('account_type');

        // Generate random data
        $salt = random_string('alnum', 25);
        $password = random_string('alnum', 16);
        $userid = $this->uuidgen->generateUUID();

        // Build the hash to be inserted from the data above
        $pass_hash = crypt($password, '$6$rounds=5452$'.$salt.'$');

        // Insert user data
        $user_data = array(
            'userID' => $userid,
            'name' => $name,
            'email' => $email,
            'acct_type' => $type
        );
        $this->sitedb->insert('admins', $user_data);

        // Insert auth data
        $auth_data = array(
            'userID' => $userid,
            'hash' => $pass_hash,
            'salt' => $salt
        );
        $this->sitedb->insert('auth', $auth_data);
    }

    public function modify_admin(){
        // Establish database connection
        $sitedb = $this->load->database('sitedb', TRUE);

        // Pull in the submitted data
        $userid = $this->input->post('userid');
        $name = $this->input->post('fullname');
        $email = $this->input->post('emailaddress');
        $type = $this->input->post('account_type');
        $password = $this->input->post('password');

        // Generate random data
        $salt = random_string('alnum', 25);

        // Build new password hash
        $pass_hash = crypt($password, '$6$rounds=5452$'.$salt.'$');
    }
}